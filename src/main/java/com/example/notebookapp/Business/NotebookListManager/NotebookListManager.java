package com.example.notebookapp.Business.NotebookListManager;

import com.example.notebookapp.DataAccess.HiberanateNotebookList.INotebookListDal;
import com.example.notebookapp.Entities.NotebookList.NotebookList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NotebookListManager implements INotebookListService {

    private INotebookListDal notebookListDal;

    @Autowired
    public NotebookListManager(INotebookListDal notebookListDal){
        this.notebookListDal = notebookListDal;
    }

    @Override
    public List<NotebookList> getList() {
        return notebookListDal.getList();
    }

    @Override
    public void addNotebook(NotebookList notebookList) {
        notebookListDal.addNotebook(notebookList);
    }

    @Override
    public NotebookList findByNotebookListId(int notebookID) {
        return notebookListDal.findByNotebookListId(notebookID);
    }
}

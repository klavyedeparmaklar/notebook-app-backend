package com.example.notebookapp.Business.NotebookListManager;

import com.example.notebookapp.Entities.NotebookList.NotebookList;

import java.util.List;

public interface INotebookListService {
    List<NotebookList> getList();
    void addNotebook(NotebookList notebookList);
    NotebookList findByNotebookListId(int notebookID);
}

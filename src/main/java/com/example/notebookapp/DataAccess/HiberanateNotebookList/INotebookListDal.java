package com.example.notebookapp.DataAccess.HiberanateNotebookList;

import com.example.notebookapp.Entities.NotebookList.NotebookList;

import java.util.List;

public interface INotebookListDal {
    void addNotebook(NotebookList notebookList);
    List<NotebookList> getList();
    NotebookList findByNotebookListId(int notebookID);
}

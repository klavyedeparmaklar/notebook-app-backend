package com.example.notebookapp.Business.TelephoneListManager;

import com.example.notebookapp.Entities.TelephoneList.TelephoneList;

import java.util.List;

public interface ITelephoneListService {
    List<TelephoneList> getList();
    void updateTelephoneList(TelephoneList telephoneList);
}

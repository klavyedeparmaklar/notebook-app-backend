package com.example.notebookapp.Entities.NotebookList;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notebookapp")
public class NotebookList {
    @Id
    @Column(name = "notebookid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int notebookID;

    @Column(name = "notebookmodel")
    private String notebookModal;

    @Column(name = "notebookserialno")
    private String notebookSerialNo;

    @Column(name = "notebookname")
    private String notebookName;

    @Column(name = "anydeskid")
    private int anydeskID;

    @Column(name = "owner")
    private String owner;

    @Column(name = "owneremail")
    private String ownerMail;

    @Column(name = "ownerotp")
    private String ownerOTP;

    @Column(name = "deliverydate")
    private Date deliveryDate;

    @Column(name = "notebookstatus")
    private String notebookStatus;

    public NotebookList() { }

    public NotebookList(int notebookID, String notebookModal, String notebookSerialNo, String notebookName, int anydeskID, String owner, String ownerMail, String ownerOTP, Date deliveryDate, String notebookStatus) {
        this.notebookID = notebookID;
        this.notebookModal = notebookModal;
        this.notebookSerialNo = notebookSerialNo;
        this.notebookName = notebookName;
        this.anydeskID = anydeskID;
        this.owner = owner;
        this.ownerMail = ownerMail;
        this.ownerOTP = ownerOTP;
        this.deliveryDate = deliveryDate;
        this.notebookStatus = notebookStatus;
    }

    public int getNotebookID() {
        return notebookID;
    }

    public void setNotebookID(int notebookID) {
        this.notebookID = notebookID;
    }

    public String getNotebookModal() {
        return notebookModal;
    }

    public void setNotebookModal(String notebookModal) {
        this.notebookModal = notebookModal;
    }

    public String getNotebookSerialNo() {
        return notebookSerialNo;
    }

    public void setNotebookSerialNo(String notebookSerialNo) {
        this.notebookSerialNo = notebookSerialNo;
    }

    public String getNotebookName() {
        return notebookName;
    }

    public void setNotebookName(String notebookName) {
        this.notebookName = notebookName;
    }

    public int getAnydeskID() {
        return anydeskID;
    }

    public void setAnydeskID(int anydeskID) {
        this.anydeskID = anydeskID;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwnerMail() {
        return ownerMail;
    }

    public void setOwnerMail(String ownerMail) {
        this.ownerMail = ownerMail;
    }

    public String getOwnerOTP() {
        return ownerOTP;
    }

    public void setOwnerOTP(String ownerOTP) {
        this.ownerOTP = ownerOTP;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getNotebookStatus() {
        return notebookStatus;
    }

    public void setNotebookStatus(String notebookStatus) {
        this.notebookStatus = notebookStatus;
    }
}

package com.example.notebookapp.DataAccess.HiberanateNotebookList;

import com.example.notebookapp.Entities.NotebookList.NotebookList;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class HibernateNotebookListDal implements INotebookListDal{
    private EntityManager entityManager;

    @Autowired
    public HibernateNotebookListDal(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public List<NotebookList> getList() {
        Session session = entityManager.unwrap(Session.class);
        List<NotebookList> listNotebook = session.createQuery("from NotebookList", NotebookList.class).getResultList();
        return listNotebook;
    }

    @Override
    @Transactional
    public void addNotebook(NotebookList notebookList) {
        Session session = entityManager.unwrap(Session.class);
        session.saveOrUpdate(notebookList);
    }

    @Override
    @Transactional
    public NotebookList findByNotebookListId(int notebookID) {
        Session session = entityManager.unwrap(Session.class);
        NotebookList notebookList = session.get(NotebookList.class, notebookID);
        return notebookList;
    }
}


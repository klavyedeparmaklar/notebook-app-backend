package com.example.notebookapp.Business.TelephoneListManager;

import com.example.notebookapp.DataAccess.HiberanateTelephoneList.ITelephoneListDal;
import com.example.notebookapp.Entities.TelephoneList.TelephoneList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.websocket.server.ServerEndpoint;
import java.util.List;

@Service
public class TelephoneListManager implements  ITelephoneListService{
    private ITelephoneListDal iTelephoneListDal;

    @Autowired
    public TelephoneListManager(ITelephoneListDal iTelephoneListDal) {
        this.iTelephoneListDal = iTelephoneListDal;
    }

    @Override
    @Transactional
    public List<TelephoneList> getList() {
        return iTelephoneListDal.getList();
    }

    @Override
    @Transactional
    public void updateTelephoneList(TelephoneList telephoneList) {
        iTelephoneListDal.updateTelephoneList(telephoneList);
    }
}

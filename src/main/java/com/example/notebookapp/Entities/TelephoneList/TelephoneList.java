package com.example.notebookapp.Entities.TelephoneList;

import javax.persistence.*;

@Entity
@Table(name="telephonelist")
public class TelephoneList {
    @Id
    @Column(name="telephoneid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int telephoneID;

    @Column(name="username")
    private String userName;

    @Column(name="title")
    private String title;

    @Column(name="extention")
    private String extention;

    @Column(name="directline")
    private String directLine;

    @Column(name="location")
    private String location;

    public TelephoneList(int telephoneID, String userName, String title, String extention, String directLine, String location) {
        this.telephoneID = telephoneID;
        this.userName = userName;
        this.title = title;
        this.extention = extention;
        this.directLine = directLine;
        this.location = location;
    }

    public TelephoneList(){}

    public int getTelephoneID() {
        return telephoneID;
    }

    public void setTelephoneID(int telephoneID) {
        this.telephoneID = telephoneID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExtention() {
        return extention;
    }

    public void setExtention(String extention) {
        this.extention = extention;
    }

    public String getDirectLine() {
        return directLine;
    }

    public void setDirectLine(String directLine) {
        this.directLine = directLine;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}


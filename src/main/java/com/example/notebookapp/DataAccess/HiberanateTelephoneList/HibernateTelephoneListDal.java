package com.example.notebookapp.DataAccess.HiberanateTelephoneList;

import com.example.notebookapp.Entities.TelephoneList.TelephoneList;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class HibernateTelephoneListDal  implements ITelephoneListDal{
    private EntityManager entityManager;

    @Autowired
    public HibernateTelephoneListDal(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public List<TelephoneList> getList() {
        Session session = entityManager.unwrap(Session.class);
        List<TelephoneList> telephoneLists = session.createQuery("from TelephoneList", TelephoneList.class).getResultList();
        return telephoneLists;
    }

    @Override
    @Transactional
    public void updateTelephoneList(TelephoneList telephoneList) {
        Session session = entityManager.unwrap(Session.class);
        session.update(telephoneList);
    }
}

package com.example.notebookapp.RestApi.NotebookListController;

import com.example.notebookapp.Business.NotebookListManager.INotebookListService;
import com.example.notebookapp.Entities.NotebookList.NotebookList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class NotebookListController {
    private INotebookListService notebookListService;

    @Autowired
    public NotebookListController(INotebookListService notebookListService){
        this.notebookListService = notebookListService;
    }

    @GetMapping("/notebooklist")
    public List<NotebookList> getList(){
        return notebookListService.getList();
    }

    @PostMapping("/addnotebook")
    public void addNotebook(@RequestBody NotebookList notebookList){
//        NotebookList unupdatedNotebookList = new NotebookList();
//        unupdatedNotebookList.setNotebookModal(notebookList.getNotebookModal());
//        unupdatedNotebookList.setNotebookSerialNo(notebookList.getNotebookSerialNo());
//        unupdatedNotebookList.setNotebookName(notebookList.getNotebookName());
//        unupdatedNotebookList.setAnydeskID(notebookList.getAnydeskID());
//        unupdatedNotebookList.setOwner(notebookList.getOwner());
//        unupdatedNotebookList.setOwnerMail(notebookList.getOwnerMail());
//        unupdatedNotebookList.setOwnerOTP(notebookList.getOwnerOTP());
//        unupdatedNotebookList.setDeliveryDate(notebookList.getDeliveryDate());
//        unupdatedNotebookList.setNotebookStatus(notebookList.getNotebookStatus());
        notebookListService.addNotebook(notebookList);
    }

    @PostMapping("/update")
    public void updateNotebookList(@RequestBody NotebookList notebookList){
        NotebookList unupdatedNotebookList = notebookListService.findByNotebookListId(notebookList.getNotebookID());
        unupdatedNotebookList.setNotebookModal(notebookList.getNotebookModal());
        unupdatedNotebookList.setNotebookSerialNo(notebookList.getNotebookSerialNo());
        unupdatedNotebookList.setNotebookName(notebookList.getNotebookName());
        unupdatedNotebookList.setAnydeskID(notebookList.getAnydeskID());
        unupdatedNotebookList.setOwner(notebookList.getOwner());
        unupdatedNotebookList.setOwnerMail(notebookList.getOwnerMail());
        unupdatedNotebookList.setOwnerOTP(notebookList.getOwnerOTP());
        unupdatedNotebookList.setDeliveryDate(notebookList.getDeliveryDate());
        unupdatedNotebookList.setNotebookStatus(notebookList.getNotebookStatus());

        notebookListService.addNotebook(unupdatedNotebookList);
    }

}

package com.example.notebookapp.RestApi.TelephoneController;

import com.example.notebookapp.Business.TelephoneListManager.ITelephoneListService;
import com.example.notebookapp.Entities.TelephoneList.TelephoneList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class TelephoneController {
    private ITelephoneListService telephoneListService;

    @Autowired
    public TelephoneController(ITelephoneListService telephoneListService) {
        this.telephoneListService = telephoneListService;
    }

    @GetMapping("/alltelephones")
    public List<TelephoneList> getList(){
        return telephoneListService.getList();
    }

    @PostMapping("/updatetelephone")
    public void updateTelephoneList(@RequestBody TelephoneList telephoneList){
        telephoneListService.updateTelephoneList(telephoneList);
    }
}

package com.example.notebookapp.DataAccess.HiberanateTelephoneList;

import com.example.notebookapp.Entities.TelephoneList.TelephoneList;

import java.util.List;

public interface ITelephoneListDal {
    List<TelephoneList> getList();
    void updateTelephoneList(TelephoneList telephoneList);
}
